const { ApolloServer } = require('apollo-server-lambda');
// const { ApolloServer } = require('apollo-server');
const typeDefs = require('./src/schema');
const resolvers = require('./src/resolvers');

const LaunchAPI = require('./src/datasources/launch');
const RocketAPI = require('./src/datasources/rocket');
const LaunchpadAPI = require('./src/datasources/launchpad');

const server = new ApolloServer({
    typeDefs,
    resolvers,
    playground: {
        endpoint: "/dev/graphql"
    },
    dataSources: () => ({
        launchAPI: new LaunchAPI(),
        rocketAPI: new RocketAPI(),
        launchpadAPI: new LaunchpadAPI()
    })
});

exports.graphqlHandler = server.createHandler({
    cors: {
        origin: "*",
        credentials: false
    },
    endpointURL: "/graphql"
});

/*server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});*/
