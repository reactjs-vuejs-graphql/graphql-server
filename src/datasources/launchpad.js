const { RESTDataSource } = require('apollo-datasource-rest');

class LaunchpadAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = 'https://api.spacexdata.com/v3/';
    }

    async getLaunchpadById(launchId) {
        return this.get(`launchpads/${launchId}`);
    }

    async getAllLaunchpads() {
        return this.get('launchpads');
    }
}

module.exports = LaunchpadAPI;
