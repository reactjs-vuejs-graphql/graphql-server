const { RESTDataSource } = require('apollo-datasource-rest');

class RocketAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = 'https://api.spacexdata.com/v3/';
    }

    async getRocketById(rocketId) {
        return this.get(`rockets/${rocketId}`);
    }

    async getAllRockets() {
        return this.get('rockets');
    }
}

module.exports = RocketAPI;
