const {gql} = require('apollo-server');

const typeDefs = gql`
    type Query {
        launches(
            pageSize: Int
            after: String
        ): LaunchConnection!
        launch(id: ID!): Launch
        allLaunches: [Launch]
        rocket(id: String!): Rocket
        rockets: [Rocket]
        launchpads: [Launchpad]
        launchpad(id: String!): Launchpad
    }
    
    type LaunchConnection {
        cursor: String!
        hasMore: Boolean!
        launches: [Launch]!
    }

    type Launchpad {
        status: String
        details: String
        wikipedia: String
        attempted_launches: Int
        successful_launches: Int
        location: LaunchpadLocation
        site_name_long: String
        vehicles_launched: [String]
        site_id: String
    }

    type LaunchpadLocation {
        name: String
        region: String
        latitude: Float
        longitude: Float
    }

    type Launch {
        id: ID!
        site: String
        mission: Mission
        rocket: RocketLaunch
        isBooked: Boolean!
        launch_success: String
        launch_year: String
        launch_date_utc: String
        details: String
        launch_site: String
        launch_site_id: String
    }

    type RocketLaunch {
        id: ID!
        name: String
        type: String
    }

    type Mission {
        name: String
        missionPatchSmall: String
        missionPatchLarge: String
    }

    type Rocket {
        wikipedia: String
        description: String
        rocket_id: String
        rocket_name: String
        stages: Int
        boosters: Int
        active: Boolean
    }
`;

module.exports = typeDefs;
